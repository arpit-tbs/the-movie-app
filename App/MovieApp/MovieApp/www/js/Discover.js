﻿var slider, per_page = 10, NewReleasePage = 1, KidsMoviesPage = 1, UpCommingPage = 1, loading = false, Type;
function DisplayDiscoverList(SliderUrl) {
    var Value = false;
    var Url = SliderUrl;

    $.ajax({
        method: "GET",
        url: Url,
        beforeSend: function () {
            $.mobile.loading('show', {
                text: "Loding...",
                textVisible: true,
                theme: 'z'
            });
            if (slider) {
                slider.destroySlider();
            }
        },
        complete: function () {
            $.mobile.loading("hide");
        },
        success: function (data) {

            //Template Added
            $("#imageSliderTemplateScript").tmpl(data.results).appendTo("#IMGSlider");

            slider = $('#IMGSlider').bxSlider({
                auto: true,
                autoControls: true,
                pager: false,
                onSliderLoad: function () {
                    $(".bxslider-wrap").css("visibility", "visible");
                }
            });
            $(".SliderClass").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                Type = "Movie";
                GetMovieInfo(DataID, Type);
            });
        },
        error: function () {
            Value = false;
            alert(Value);
        }
    });
}

function DisplayDramaCurrentYear(ListUrl, ElementVal) {
    var Value = false;
    var Url = ListUrl;
    loading = true;
    $.ajax({
        method: "GET",
        url: Url,
        beforeSend: function () {
            $.mobile.loading('show', {
                text: "Loding...",
                textVisible: true,
                theme: "z"
            });
        },
        complete: function () {
            $.mobile.loading("hide");
            loading = false;
        },
        cache: false,
        success: function (data) {
            if (ElementVal) {
                //tab template added.
                //TODO: call CheckImageNull function
                
                $("#discoverTabTemplateScript").tmpl(data.results).appendTo("#" + ElementVal + " .ui-grid-b");
                $(".tabImage").lazyload();
            }
            $(".col-xs-4").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                Type = "Movie";
                GetMovieInfo(DataID, Type);
            });
        },
        error: function () {
            Value = false;
            alert(Value);
        }
    });
}

function CheckImageNull(PathPoster, Path) {
    if (PathPoster != null) {
        return "https://image.tmdb.org/t/p/w640" + PathPoster;
    } else {
        if (Path === "backdrop_path") {
            return "img/no-image-avalible-_landscape.jpg";
        }
        else {
            return "img/no-image-found.jpg";
        }
    }
}

$(document).on("pageshow", "#Discover", function () {
    $("#GridMovieInfo .ui-grid-b").html("");
    $("#GridTvInfo .ui-grid-b").html("");
    $("#MovieInfoFilter").val("");
    if (sessionStorage['activePage']) {
        $("#" + sessionStorage['activePage']).trigger("click");
    } else {
        SliderUrl = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=a33fd3d22f92753b98bf87a34d01acf6";
        DisplayDiscoverList(SliderUrl);
        if (NewReleasePage == 1) {
            ElementVal = "NewRelease";
            var ListUrl = "https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2016&api_key=a33fd3d22f92753b98bf87a34d01acf6" + '&page=1';
            DisplayDramaCurrentYear(ListUrl, ElementVal);
            NewReleasePage = 2;
        }
    }
    $('[data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
    $("#myPanel").panel({
        close: function (event, ui) {
            $('[data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
        }
    });
});

$(document).on("pagecreate", "#Discover", function () {
    $(document).scroll(function () {
        if ($("#Discover").hasClass("ui-page-active")) {

            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
                if (!loading) {
                    sessionStorage['activePage'] = $("ul.ui-tabs-nav .ui-state-active a").attr("id");
                    //ElementVal = sessionStorage['activePage'];
                    if (sessionStorage['activePage'] === "PopularKids") {
                        if (KidsMoviesPage >= 2) {
                            KidsMoviesPage = KidsMoviesPage - 1;
                        }
                        var ListUrl = "https://api.themoviedb.org/3/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=a33fd3d22f92753b98bf87a34d01acf6&page=" + (++KidsMoviesPage);
                        DisplayDramaCurrentYear(ListUrl, ElementVal);
                        if (KidsMoviesPage >= 2) {
                            KidsMoviesPage = KidsMoviesPage + 1;
                        }
                    }
                    else if (sessionStorage['activePage'] === "CommingUp") {
                        if (UpCommingPage >= 2) {
                            UpCommingPage = UpCommingPage - 1;
                        }
                        var ListUrl = "https://api.themoviedb.org/3/movie/upcoming?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&page=" + (++UpCommingPage);
                        DisplayDramaCurrentYear(ListUrl, ElementVal);
                        if (UpCommingPage >= 2) {
                            UpCommingPage = UpCommingPage + 1;
                        }
                    }
                    else {
                        if (NewReleasePage >= 2) {
                            NewReleasePage = NewReleasePage - 1;
                        }
                        var ListUrl = "https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2016&api_key=a33fd3d22f92753b98bf87a34d01acf6&page=" + (++NewReleasePage);
                        DisplayDramaCurrentYear(ListUrl, ElementVal);
                        if (NewReleasePage >= 2) {
                            NewReleasePage = NewReleasePage + 1;
                        }
                    }
                }
                console.log("page one");
            }
        }
    });

    $("#PopularKids").on("click", function () {
        if (slider) {
            slider.destroySlider();
        }
        $("#IMGSlider").html("");
        ElementVal = 'KidsMovies';
        SliderUrl = "https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22&api_key=a33fd3d22f92753b98bf87a34d01acf6";
        DisplayDiscoverList(SliderUrl);
        if (KidsMoviesPage == 1) {
            ListUrl = "https://api.themoviedb.org/3/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=a33fd3d22f92753b98bf87a34d01acf6" + '&page=1';
            DisplayDramaCurrentYear(ListUrl, ElementVal);
            KidsMoviesPage = 2;
        }
    });
    $("#ReleaseNew").on("click", function () {
        if (slider) {
            slider.destroySlider();
        }
        $("#IMGSlider").html("");
        ElementVal = 'NewRelease';
        SliderUrl = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=a33fd3d22f92753b98bf87a34d01acf6";
        ListUrl = "https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2016&api_key=a33fd3d22f92753b98bf87a34d01acf6" + '&page=1';
        DisplayDiscoverList(SliderUrl);
    });
    $("#CommingUp").on("click", function () {
        if (slider) {
            slider.destroySlider();
        }
        $("#IMGSlider").html("");
        ElementVal = 'UpComming';
        SliderUrl = "https://api.themoviedb.org/3/discover/movie?certification_country=US&certification=R&sort_by=revenue.desc&with_cast=3896&api_key=a33fd3d22f92753b98bf87a34d01acf6";
        DisplayDiscoverList(SliderUrl);
        if (UpCommingPage == 1) {
            ListUrl = "https://api.themoviedb.org/3/movie/upcoming?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US" + "&page=1";
            DisplayDramaCurrentYear(ListUrl, ElementVal);
            UpCommingPage = 2;
        }
    });
    $("[data-role='panel']").enhanceWithin().panel();
});



