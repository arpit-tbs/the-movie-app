﻿var load = false, LoadingKeyword = false, KeywordMoviePageList = 1, KeywordMoviePageGrid = 1, ID, NameKey, ListPage = 1, GridPage = 1, Scrolling = false, page = 1;

function GetKeywirdMovieInfo(Id, Name, ListType, Url, Scrolling, NewCall) {
    var Value = false;
     load = true;
    sessionStorage["ID"] = Id;
    ID = Id;
    NameKey = Name;
    if (NewCall == true) {
        ListPage = 1;
        GridPage = 1;
    }
    if (Scrolling == true) {
        if ($("#MovieKeywordList").hasClass("ui-icon-bullets")) {
            if (ListPage == 1) {
                var Url = 'https://api.themoviedb.org/3/keyword/' + Id + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=' + ListPage
            }
        } else {
            if (GridPage == 1) {
                var Url = 'https://api.themoviedb.org/3/keyword/' + Id + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=' + GridPage
            }
        }
    } else {    
        if ($("#MovieKeywordList").hasClass("ui-icon-bullets")) {
            if (ListPage == 1) {
                var Url = 'https://api.themoviedb.org/3/keyword/' + Id + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=1'
            }
        } else {
            if (GridPage == 1) {
                var Url = 'https://api.themoviedb.org/3/keyword/' + Id + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=1'
            }
        }
    }
    ID = Id;
    NameKey = Name;
    $.ajax({
        method: "GET",
         url: Url,
         beforeSend: function () {
             fSpinner('show');
         },
        complete: function () {
            load = false;
            fSpinner('hide');
        },
        cache: false,
        success: function (data) {
            for (var i = 0; i < data.results.length; i++) {
                if ($("#MovieKeywordList").hasClass("ui-icon-bullets")) {
                    $("#KeywordFindInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="ClassList"><a href="#DiscoverMovieDetail"><div class="ui-block-a"><div id="WidthImg"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></div></div><div class="ui-block-b"><div id="DivTitleKeyword"><strong>' + data.results[i].original_title + '</strong><span> (' + data.results[i].release_date + ') </span><p id="SortDesc"> ' + data.results[i].overview + ' </p></div><span class="ui-corner-all"> ' + CheckAvgZero(data.results[i].vote_average) + ' </span></div></a></div>')
                    $("#KeywordFindInfo .ui-grid-b img").css({ margin: "11px 14px -5px 10px", width: "80px" });
                } else {
                    $("#KeywordFindInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="col-xs-4"><a href="#DiscoverMovieDetail"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></a></div>')
                }
            }
            $(".col-xs-4").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                Type = "Movie";
                GetMovieInfo(DataID, Type);
            });
            $(".ClassList").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                Type = "Movie";
                GetMovieInfo(DataID, Type);
            });
        },
        error: function () {
            Value = false;
            alert(Value);
        }
    });
}

$(document).on("pagebeforeshow", "#FindKeywordMovie", function () {
    $("#FindKeywordMovie [data-role='header'] h1").html(sessionStorage["DataName"]);
});

$(document).on("pageshow", "#FindKeywordMovie", function () {
    $('[data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
    $("#myPanel").panel({
        close: function (event, ui) {
            $('[data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
        }
    });
});

$(document).on("pagecreate", "#FindKeywordMovie", function () {
    $(document).scroll(function () {
        if ($("#FindKeywordMovie").hasClass("ui-page-active")) {
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
                if (!load) {
                    if ($("#MovieKeywordList").hasClass("ui-icon-bullets")) {
                        Scrolling = true;
                        var Url = 'https://api.themoviedb.org/3/keyword/' + sessionStorage["ID"] + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=' + (++ListPage)
                        ListType = "GriView";
                        GetKeywirdMovieInfo(sessionStorage["ID"], NameKey, ListType, Url, Scrolling, false)
                    } else {
                        Scrolling = true;
                        ListType = "GriView";
                        var Url = 'https://api.themoviedb.org/3/keyword/' + sessionStorage["ID"] + '/movies?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&include_adult=false&page=' + (++GridPage)
                        GetKeywirdMovieInfo(sessionStorage["ID"], NameKey, ListType, Url, Scrolling, false)
                    }
                }
            }
        }
    });
    $("#MovieKeywordList").on("click", function () {
        var ListType = "";
        if ($("#MovieKeywordList").hasClass("ui-icon-bullets")) {
            $("#MovieKeywordList").addClass("ui-icon-grid").removeClass("ui-icon-bullets");
            $("#KeywordFindInfo .ui-grid-b").html("");
            ListType = "GriView";
            GetKeywirdMovieInfo(ID, NameKey, ListType, "",false, true)
        } else {
            $("#MovieKeywordList").addClass("ui-icon-bullets").removeClass("ui-icon-grid");
            $("#KeywordFindInfo .ui-grid-b").html("");
            ListType = "ListView";
            GetKeywirdMovieInfo(ID, NameKey, ListType, "",false, true)
        }
    });
    $("[data-role='panel']").enhanceWithin().panel();
});

