﻿
function CheckImageNull(PathPoster, Path) {
    if (PathPoster != null) {
        return PathPoster = "https://image.tmdb.org/t/p/w640" + PathPoster;
    } else {
        if (Path == "backdrop_path") {
            return PathPoster = "img/no-image-avalible-_landscape.jpg";
        }
        else {
            return PathPoster = "img/no-image-found.jpg";
        }
    }
}

function currencyFormat(num) {
    return "$" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function CheckAvgZero(AvgValue) {
    if (AvgValue == 0) {
        return AvgValue = "Not Available";
    } else {
        return AvgValue = parseFloat(AvgValue).toFixed(1);
    }
}

function HasExitsDate(HasDate) {
    if (HasDate != "") {
        return HasDate;
    } else {
        return HasDate = "No Date";
    }
}


function fSpinner(strShowOrHide) {
    setTimeout(function () {
        $.mobile.loading(strShowOrHide);
    }, 2);
}
