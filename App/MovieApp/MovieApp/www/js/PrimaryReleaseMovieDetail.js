﻿
function PrimaryReleaseMovieInfo(id) {
    sessionStorage['activePage'] = $("ul.ui-tabs-nav .ui-state-active a").attr("id");
    $.ajax({
        method: "GET",
        url: "https://api.themoviedb.org/3/movie/" + id + "?api_key=a33fd3d22f92753b98bf87a34d01acf6",
        success: function (data) {
            $("#PrimaryBackdropPath").attr("src", 'https://image.tmdb.org/t/p/w640' + data.backdrop_path);
            $("#imgDiv img").attr("src", 'https://image.tmdb.org/t/p/w640' + data.poster_path);
            $("#PrimaryReleaseMovieDetailPage").find("#PrimaryTitleId").html(data.title);

            var LengTitle = $("#PrimaryTitleId").html();
            if (LengTitle.length > 21) {
                $("#VoteAveDiv p").css("margin-top", "21px")
            }
            else {
                $("#VoteAveDiv p").css("margin-top", "47px")
            }
            var genreName = [];
            for (var i = 0; i < data.genres.length; i++) {
                genreName.push(" " + data.genres[i].name);
            }
            $("#PrimaryGenresName").html('<li><a class="rounded" href="#"></a>' + genreName + '</li>');

            $("#PrimaryOverView").find("p").html(data.overview);
            $("#PrimaryStatus").html(data.status);
            $("#PrimaryOriginalLanguage").html(data.original_language);
            $("#PrimaryRuntime").html(data.runtime);
            $("#PrimaryBudget").html(currencyFormat(data.budget));
            $("#PrimaryRevenue").html(currencyFormat(data.revenue));
            $("#PrimaryHomePage").html(data.homepage);
            $("#PrimaryVoteAverage").html(parseFloat(data.vote_average).toFixed(1));
            //ShowHeight();
            
        },
        error: function () {
            alert("Error");
        }
    });
}

function currencyFormat(num) {
    return "$" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$(document).on("pageshow", "#PrimaryReleaseMovieDetailPage", function () {

});

$(document).on("pagecreate", "#PrimaryReleaseMovieDetailPage", function () {

});

//function ShowHeight() {
//    var LengTitle = $("#PrimaryTitleId").html();
//    if (LengTitle.length > 21) {
//        $("#VoteAveDiv p").css("margin-top", "21px")
//    }
//    else {
//        $("#VoteAveDiv p").css("margin-top", "47px")
//    }
//}

