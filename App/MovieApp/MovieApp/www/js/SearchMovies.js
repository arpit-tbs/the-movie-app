﻿var load = false, Page = 1, KeywordName = "", MovieType, SeaMoviePageGrid = 1, SeaMoviePageList = 1, TvSearchPageGrid = 1, TvSearchPageList = 1, ActiveTab;

function SearchMovieInfo(ListUrl, ActiveTab) {
    var Url = ListUrl;
    load = true;
    $.ajax({
        method: "GET",
        url: Url,
        beforeSend: function () {
            $.mobile.loading('show', {
                text: "Loding...",
                textVisible: true,
                theme: 'z'
            });
        },
        complete: function () {
            $.mobile.loading("hide");
            load = false;
        },
        cache: false,
        success: function (data) {
            $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
            for (var i = 0; i < data.results.length; i++) {
                if ($("#MovieList").hasClass("ui-icon-bullets")) {
                    if (ActiveTab == "MovieSearch") {
                        $("#GridMovieInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="ListClass"><a href="#DiscoverMovieDetail"><div class="ui-block-a"><div id="ImgWidth"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></div></div><div class="ui-block-b"><div id="DivTitle"><strong>' + data.results[i].original_title + '</strong><span> (' + data.results[i].release_date + ') </span><p id="SortDesc"> ' + data.results[i].overview + ' </p></div><span class="ui-corner-all"> ' + CheckAvgZero(data.results[i].vote_average) + ' </span></div></a></div>')
                        $("#GridMovieInfo .ui-grid-b img").css({ margin: "0px 14px 6px 10px", width: "80px" });
                    } else {
                        $("#GridTvInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="ListClass"><a href="#DiscoverMovieDetail"><div class="ui-block-a"><div id="ImgWidth"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></div></div><div class="ui-block-b"><div id="DivTitle"><strong>' + data.results[i].original_name + '</strong><span> (' + HasExitsDate(data.results[i].first_air_date) + ') </span><p id="SortDesc"> ' + data.results[i].overview + ' </p></div><span class="ui-corner-all"> ' + CheckAvgZero(data.results[i].vote_average) + ' </span></div></a></div>')
                        $("#GridTvInfo .ui-grid-b img").css({ margin: "0px 14px 6px 10px", width: "80px" });
                    }
                } else {
                    if (ActiveTab == "MovieSearch") {
                        $("#GridMovieInfo .ui-grid-b img").css("width", "80px");
                        $("#GridMovieInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="col-xs-4"><a href="#DiscoverMovieDetail"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></a></div>')
                    } else {
                        $("#GridTvInfo .ui-grid-b img").css("width", "80px");
                        $("#GridTvInfo .ui-grid-b").append('<div  data-id="' + data.results[i].id + '" class="col-xs-4"><a href="#DiscoverMovieDetail"><img src="' + CheckImageNull(data.results[i].poster_path, "poster_path") + '" /></a></div>')
                    }
                }
            }

            $(".col-xs-4").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                var CheckActive = $("#SearchMovie ul.ui-tabs-nav .ui-state-active a").attr("id");
                if (CheckActive == "SeaMovie") {
                    Type = "Movie";
                } else {
                    Type = "Tv";
                }
                GetMovieInfo(DataID, Type);
            });
            $(".ListClass").off("click").on("click", function () {
                var DataID = $(this).attr("data-id");
                var CheckActive = $("#SearchMovie ul.ui-tabs-nav .ui-state-active a").attr("id");
                if (CheckActive == "SeaMovie") {
                    Type = "Movie";
                } else {
                    Type = "Tv";
                }
                GetMovieInfo(DataID, Type);
            });
        },
        error: function () {
            alert("Error");
        }
    });
}

$(document).on("pageshow", "#SearchMovie", function () {

    $("#MovieInfoFilter").keypress(function (event) {
        $("#GridMovieInfo .ui-grid-b").html("");
        $("#GridTvInfo .ui-grid-b").html("");
        console.log("Key pressed");
        if (event.keyCode == 13) {
            KeywordName = document.getElementById("MovieInfoFilter").value;
            var TabActive = $("#SearchMovie ul.ui-tabs-nav .ui-state-active a").attr("id");
            if (TabActive == "SeaMovie") {
                ActiveTab = "MovieSearch";
                var ListUrl = 'https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=1&include_adult=false'
                SearchMovieInfo(ListUrl, ActiveTab);
            } else {
                ActiveTab = "TvSearch";
                var ListUrl = 'https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=1&include_adult=false'
                SearchMovieInfo(ListUrl, ActiveTab);
            }
            if (event.which === 13) {
                $(this).blur();
            }
        }
    });

    $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
    $("#myPanel").panel({
        close: function (event, ui) {
            $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
        }
    });
});

$(document).on("pagecreate", "#SearchMovie", function () {
    $(document).scroll(function () {

        if ($("#SearchMovie").hasClass("ui-page-active")) {
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
                if (!load) {
                    sessionStorage['activePage'] = $("#SearchMovie ul.ui-tabs-nav .ui-state-active a").attr("id");
                     KeywordName = document.getElementById("MovieInfoFilter").value;
                    if (sessionStorage['activePage'] == "SearchTV") {
                        if ($("#MovieList").hasClass("ui-icon-bullets")) {
                            var ListUrl = 'https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=' + (++TvSearchPageList) + '&include_adult=false'
                            SearchMovieInfo(ListUrl, ActiveTab);
                        } else {
                            var ListUrl = 'https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=' + (++TvSearchPageGrid) + '&include_adult=false'
                            SearchMovieInfo(ListUrl, ActiveTab);
                        }
                    }
                    else {
                        if ($("#MovieList").hasClass("ui-icon-bullets")) {
                            var ListUrl = 'https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=' + (++SeaMoviePageList) + '&include_adult=false'
                            SearchMovieInfo(ListUrl, ActiveTab);
                        } else {
                            var ListUrl = 'https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=' + KeywordName + '&page=' + (++SeaMoviePageGrid) + '&include_adult=false'
                            SearchMovieInfo(ListUrl, ActiveTab);
                        }
                    }
                }
            }

        }
    });
    $("#MovieList").on("click", function () {
        $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
        $("#GridMovieInfo .ui-grid-b").html("");
        $("#GridTvInfo .ui-grid-b").html("");
        KeywordName = document.getElementById("MovieInfoFilter").value;
        if (KeywordName != "") {
            if ($("#MovieList").hasClass("ui-icon-bullets")) {
                $("#MovieList").addClass("ui-icon-grid").removeClass("ui-icon-bullets");
               $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
                if (ActiveTab == "MovieSearch") {
                    var ListUrl = "https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
                } else {
                    var ListUrl = "https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
                }
            } else {
                $("#MovieList").addClass("ui-icon-bullets").removeClass("ui-icon-grid");
                $('#SearchMovie [data-role="navbar"]').find("li.ui-tabs-active a").addClass("ui-btn-active");
                if (ActiveTab == "MovieSearch") {
                    var ListUrl = "https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
                } else {
                    var ListUrl = "https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
                }
            }
            SearchMovieInfo(ListUrl, ActiveTab);
        }
    });

    $("#SeaMovie").on("click", function () {
        $(this).addClass("ui-btn-active");
        KeywordName = document.getElementById("MovieInfoFilter").value
        if (KeywordName != "") {
            ActiveTab = "MovieSearch";
            $("#GridMovieInfo .ui-grid-b").html("");
            $("#GridTvInfo .ui-grid-b").html("");
            var ListUrl = "https://api.themoviedb.org/3/search/movie?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
            SearchMovieInfo(ListUrl, ActiveTab);
        } else {
            $("#GridMovieInfo .ui-grid-b").html("");
            $("#GridTvInfo .ui-grid-b").html("");
        }
    });

    $("#SearchTV").on("click", function () {
        $(this).addClass("ui-btn-active");
        KeywordName = document.getElementById("MovieInfoFilter").value
        if (KeywordName != "") {
            ActiveTab = 'TvSearch';
            $("#GridMovieInfo .ui-grid-b").html("");
            $("#GridTvInfo .ui-grid-b").html("");
            var ListUrl = "https://api.themoviedb.org/3/search/tv?api_key=a33fd3d22f92753b98bf87a34d01acf6&language=en-US&query=" + KeywordName + "&page=1&include_adult=false";
            SearchMovieInfo(ListUrl, ActiveTab);
        }
        else {
            $("#GridMovieInfo .ui-grid-b").html("");
            $("#GridTvInfo .ui-grid-b").html("");
        }
    });
    $("[data-role='panel']").enhanceWithin().panel();
});
