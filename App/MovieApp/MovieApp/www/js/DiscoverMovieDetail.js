﻿var FlagLoading = false, NewCall = false;
function GetMovieInfo(id, Type) {
    var Url = "";
    if (Type == "Movie") {
        Url = 'https://api.themoviedb.org/3/movie/ ' + id + ' ?api_key=a33fd3d22f92753b98bf87a34d01acf6&append_to_response=credits,keywords,images&include_image_language=en,null';
    } else {
        Url = 'https://api.themoviedb.org/3/tv/ ' + id + ' ?api_key=a33fd3d22f92753b98bf87a34d01acf6';
    }
    sessionStorage['activePage'] = $("ul.ui-tabs-nav .ui-state-active a").attr("id");
    $.ajax({
        method: "GET",
        url: Url,
        beforeSend: function () {
            FlagLoading = true;
        },
        complete: function () {
            $.mobile.loading("hide");
            FlagLoading = false;
        },
        async: false,
        success: function (data) {
            ClearData();
            var BackDropPath = CheckImageNull(data.backdrop_path, "backdrop_path");
            $("#BackdropPath").attr("src", BackDropPath);
            $("#imgDiv img").attr("src", 'https://image.tmdb.org/t/p/w640' + data.poster_path);
            if (Type == "Movie") {
                $("#DiscoverMovieDetail").find("#titleID").html(data.title);
                sessionStorage["DataTitleName"] = data.title;

            } else {
                $("#DiscoverMovieDetail").find("#titleID").html(data.name);
            }
            var genreName = [];
            for (var i = 0; i < data.genres.length; i++) {
                $("#genresName").append('<li><a data-id="' + data.genres[i].id + ' "    data-name=' + data.genres[i].name + ' data-role="none" class="GenreClass"  href="#FindGenreMovie">' + data.genres[i].name + ", " + '</a></li>');
            }
            var str = $("#genresName li:last-child").find("a").html();
            str = str.replace(/,\s*$/, "");
            $("#genresName li:last-child a").html(str);


            $(".GenreClass").off("click").on("click", function () {
                $("#GenreFindInfo .ui-grid-b").html("");
                NewCall = true;
                var ID = $(this).attr("data-id");

                var Name = $(this).find("a").text();

                sessionStorage["DataName"] = Name;
                GetGenreMovieInfo(ID, Name, "", "", false, NewCall);
            });
            $("#OverView").find("p").html(data.overview);
            $("#Status").html(data.status);
            if (Type == "Movie") {
                $("#OriginalLanguage").html(data.spoken_languages[0].name);
            } else {
                $("#OriginalLanguage").html(data.languages);
            }
            var time = "";
            if (data.runtime != null) {
                var allMinutes = data.runtime;
                var hours = Math.floor(allMinutes / 60);
                var minutes = allMinutes % 60;
                var time = hours + "h " + minutes + "m";

            } else {
                time = "Not Available";
            }
            $("#Runtime").html(time);
            if (Type == "Movie") {
                $("#Budget").html(currencyFormat(data.budget));
                $("#Revenue").html(currencyFormat(data.revenue));
            }
            $("#HomePage").html(data.homepage);
            $("#VoteAverage").html(CheckAvgZero(data.vote_average));
            $("#ReleaseDate").html(data.release_date);
            $("#keywords").html("");

            for (var i = 0; i < data.keywords.keywords.length; i++) {
                $("#keywords").append('<a data-id="' + data.keywords.keywords[i].id + ' "    data-name=' + data.keywords.keywords[i].name + ' data-role="none" class="rounded" href="#FindKeywordMovie"><span class="ui-corner-all" > ' + data.keywords.keywords[i].name + ' </span></a>')
            }
            $(".rounded").off("click").on("click", function () {
                $("#KeywordFindInfo .ui-grid-b").html("");
                NewCall = true;
                var ID = $(this).attr("data-id");

                var Name = $(this).find("span").text();

                sessionStorage["DataName"] = Name;
                GetKeywirdMovieInfo(ID, Name, "", "", false, NewCall);
            });


            $("#mediaSliderTemplateScript").tmpl(data.images.posters).appendTo("#mediaSlider");




            $("#MovieCastInfo .ui-grid-b").html("");
            var divclass = "";
            for (var i = 0; i < data.credits.cast.length; i++) {
                $("#MovieCastInfo .ui-grid-b").append('<div  data-id="' + data.credits.cast[i].id + '" class="ListClass"><a href="#DiscoverMovieDetail"><div class="ui-block-a"><div id="ImgWidth"><img class="img-round" src="' + CheckImageNull(data.credits.cast[i].profile_path, "poster_path") + '" /></div></div><div class="ui-block-b"><div id="DivName"><strong>' + data.credits.cast[i].name + '</strong><p> ' + data.credits.cast[i].character + '</p></div></div></a></div>')
                $("#MovieCastInfo .ui-grid-b img").css({ margin: "2px 8px 7px 1px", width: "80px" });
            }
        },
        error: function () {
            alert("Error");
        }
    });
}

$(document).on("pageshow", "#DiscoverMovieDetail", function () {
    $("#set").children(":last").collapsible("collapse");
    if (FlagLoading === true) {
        $.mobile.loading('show', {
            text: "Loding...",
            textVisible: true,
            theme: 'z'
        });
    }
    $('#mediaSlider').bxSlider({
        auto: true,
        autoControls: true,
        pager: false,
        onSliderLoad: function () {
            $(".bxslider-wrap").css("visibility", "visible");
        }
    });
});

$(document).on("pagecreate", "#DiscoverMovieDetail", function () {
});

function ClearData() {
    $("#BackdropPath").attr("src", "");
    $("#imgDiv img").attr("src", "")
    $("#DiscoverMovieDetail").find("#titleID").html("");
    $("#genresName").html("");
    $("#OverView").find("p").html("");
    $("#Status").html("");
    $("#OriginalLanguage").html("");
    $("#Runtime").html("");
    $("#Budget").html("");
    $("#Revenue").html("");
    $("#HomePage").html("");
    $("#VoteAverage").html("");
    $("#mediaSlider").html("");
}



